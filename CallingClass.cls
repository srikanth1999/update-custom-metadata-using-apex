public with sharing class CallingClass {


    @AuraEnabled
    public static Boolean setCIFMetadata(List<CIF_Form_Wizard_Configuration__mdt> metadataList) {
        try {
            MetaDataAPIUtility.upsertMetadata(metadataList);
            return true;
        } catch(Exception ex){
            System.debug('Exception ---->'+ex.getMessage());
            return false;
        }
    }

}