public without sharing class MetaDataAPIUtility {

    public static String upsertMetadata(List<sObject> customMetadataList ) {
    
        //Create Deployment container for custom Metadata
        Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
        for(sobject sObMD : customMetadataList) {
            
            //Get metadata object name and details
            String sObjectname = sObMD.getSObjectType().getDescribe().getName();
            
            //Create custom Metadata instance
            Metadata.CustomMetadata customMetadata =  new Metadata.CustomMetadata();
            String recordName = String.valueOf(sObMD.get('DeveloperName')).replaceAll(' ','_');
            customMetadata.fullName = sObjectname +'.'+recordName;
            customMetadata.label = (String)sObMD.get('MasterLabel');
            
            //Get all fields
            schema.SObjectType sobjType = Schema.getGlobalDescribe().get(sObjectname );
            
            Map<String, Schema.sObjectField> sObjectFields = sobjType.getDescribe().fields.getMap();
            Set<String> skipFieldSet = new Set<String>{'developername','masterlabel','language','namespaceprefix', 'label','qualifiedapiname', 'id'};
            
            // Use getPopulatedFieldsAsMap to get the populate field and iterate over them
            for(String fieldName : sObMD.getPopulatedFieldsAsMap().keySet()) {
                if(skipFieldSet.contains(fieldName.toLowerCase())|| sObMD.get(fieldName) == null)
                    continue;
                
                Object value = sObMD.get(fieldName);    
                //create field instance and populate it with field API name and value
                Metadata.CustomMetadataValue customField = new Metadata.CustomMetadataValue();
                customField.field = fieldName;
            
                Schema.DisplayType valueType = sObjectFields.get(fieldName).getDescribe().getType();
                if (value instanceof String && valueType != Schema.DisplayType.String)
                {
                    String svalue = (String)value;
                    if (valueType == Schema.DisplayType.Double)
                        customField.value = Double.valueOf(svalue);
                    else if (valueType == Schema.DisplayType.Integer)
                        customField.value = Integer.valueOf(svalue);
                    else
                        customField.value = svalue;
                }
                else
                    customField.value = value;
                    customMetadata.values.add(customField);
            }
            //Add metadata in container
            mdContainer.addMetadata(customMetadata);
        }
    
        // Callback class instance
        CustomMetadataCallback callback = new CustomMetadataCallback();
        Id jobId;
        if(!Test.isRunningTest()) {
            jobId = Metadata.Operations.enqueueDeployment(mdContainer, callback);
        }
        return jobId;
    }
}